/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.Organization.DonorOrganization;
import Business.Organization.NurseOrganization;
import Business.Organization.Organization;
import Business.Systemm;
import Business.UserAccounts.UserAccounts;
import Interface.Organization.DonorOrganizationArea;
import Interface.Organization.NurseOrganizationArea;
import javax.swing.JPanel;

/**
 *
 * @author Ali
 */
public class NurseRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccounts account, Organization organization, Enterprise enterprise, Systemm business) {
        return new NurseOrganizationArea(userProcessContainer, account, (NurseOrganization)organization, enterprise,business);
    }
    
    
}
