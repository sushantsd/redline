/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.Organization.RequestManagerOrganization;
import Business.Systemm;
import Business.UserAccounts.UserAccounts;
import Interface.Organization.RequestManagerArea;
import javax.swing.JPanel;
import userinterface.bloodcamp.mapp;

/**
 *
 * @author Ali
 */
public class RequestManagerRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccounts account, Organization organization, Enterprise enterprise, Systemm business) {
        return new mapp(userProcessContainer, account, (RequestManagerOrganization)organization, enterprise,business);
    }
    
    
}
