/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccounts.UserAccounts;
import javax.swing.JPanel;
import Business.Systemm;




public abstract class Role {
    
     private String roleType;
    
    public enum RoleType{
         
     //SystemAdmin("System Administrator"),
    // CDCEnterpriseAdmin("CDC Enterprise Admin"),
    // DistributorEnterpriseAdmin("Distributor Enterprise Admin"),
        // Admin("Admin"),
        Doctor("Doctor"),
        LabAssistant("Lab Assistant"),
        Donor("Donor"),
        Nurse("Nurse");
     
    
       
          private String value;
        private RoleType(String value){
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
    
    
    public abstract JPanel createWorkArea(JPanel userProcessContainer, 
            UserAccounts account, 
            Organization organization, 
            Enterprise enterprise, 
            Systemm business);

    @Override
    public String toString() {
       // return this.getClass().getTypeName();
      return this.getClass().getName();
    }
     
}
    