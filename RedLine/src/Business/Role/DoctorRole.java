/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;

import Business.Organization.DoctorOrganization;
import Business.Organization.Organization;
import Business.Systemm;
import Business.UserAccounts.UserAccounts;
import Interface.Organization.DoctorOrganizationArea;


import javax.swing.JPanel;




public class DoctorRole extends Role {

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccounts account, Organization organization, Enterprise enterprise, Systemm business) {
        return new DoctorOrganizationArea(userProcessContainer, account, (DoctorOrganization)organization, enterprise,business);
    }
    

    


    
    
}
