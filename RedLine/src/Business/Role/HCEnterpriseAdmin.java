/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.Organization.Organization;
import Business.UserAccounts.UserAccounts;
import Interface.Enterprise.HCEnterpriseWorkArea;
import javax.swing.JPanel;


public class HCEnterpriseAdmin extends Role{
 
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccounts account, Organization organization, Enterprise enterprise, Business.Systemm business) {
        return new HCEnterpriseWorkArea(userProcessContainer, enterprise);
    }
    
}
