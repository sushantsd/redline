/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.Enterprise.EnterpriseDirectory;
import Business.Inventory.IinventoryDirectory;

/**
 *
 * @author Shreya R
 */

public class Network {
  private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private IinventoryDirectory inventoryDirectory;

    public IinventoryDirectory getInventoryDirectory() {
        return inventoryDirectory;
    }

    public void setInventoryDirectory(IinventoryDirectory inventoryDirectory) {
        this.inventoryDirectory = inventoryDirectory;
    }
    

    public Network() {
       
        enterpriseDirectory = new EnterpriseDirectory();
         inventoryDirectory= new IinventoryDirectory();
    }
    

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
