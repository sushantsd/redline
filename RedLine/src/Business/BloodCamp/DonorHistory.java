/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodCamp;

import Business.UserAccounts.UserAccounts;

/**
 *
 * @author harshini
 */
public class DonorHistory {
    
    private DonorRegistration dr;
   // private int Donorid;
    private Blood blood;
    private boolean aids;
    private boolean cancer;
    private String sugar;
    private String rbc;
    private boolean hepb;
    private boolean hepc;
    private String elisa;
    private String remarks;
    private UserAccounts useraccount;
    private String QRcodePath;

    public String getQRcodePath() {
        return QRcodePath;
    }

    public void setQRcodePath(String QRcodePath) {
        this.QRcodePath = QRcodePath;
    }
    
     public DonorHistory(){
       //  Donorid = count;
       // count++;
       this.blood= new Blood();
       this.useraccount= new UserAccounts();
       this.dr=new DonorRegistration();
    }


    public DonorRegistration getDr() {
        return dr;
    }

    public void setDr(DonorRegistration dr) {
        this.dr = dr;
    }

    public Blood getBlood() {
        return blood;
    }

    public void setBlood(Blood blood) {
        this.blood = blood;
    }

    public boolean isAids() {
        return aids;
    }

    public void setAids(boolean aids) {
        this.aids = aids;
    }

    public boolean isCancer() {
        return cancer;
    }

    public void setCancer(boolean cancer) {
        this.cancer = cancer;
    }

    public String getSugar() {
        return sugar;
    }

    public void setSugar(String sugar) {
        this.sugar = sugar;
    }

    public String getRbc() {
        return rbc;
    }

    public void setRbc(String rbc) {
        this.rbc = rbc;
    }

    public boolean isHepb() {
        return hepb;
    }

    public void setHepb(boolean hepb) {
        this.hepb = hepb;
    }

    public boolean isHepc() {
        return hepc;
    }

    public void setHepc(boolean hepc) {
        this.hepc = hepc;
    }

    public String getElisa() {
        return elisa;
    }

    public void setElisa(String elisa) {
        this.elisa = elisa;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public UserAccounts getUseraccount() {
        return useraccount;
    }

    public void setUseraccount(UserAccounts useraccount) {
        this.useraccount = useraccount;
    }

  
}
