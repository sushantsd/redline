/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodCamp;

//import Business.BloodBank.BloodBank;
import java.util.ArrayList;

/**
 *
 * @author harshini
 */
public class BloodCampDir {
    private ArrayList<BloodCamp> bloodcampList;

    public ArrayList<BloodCamp> getBloodcampList() {
        return bloodcampList;
    }

    public void setBloodcampList(ArrayList<BloodCamp> bloodcampList) {
        this.bloodcampList = bloodcampList;
    }
    public BloodCampDir(){
        this.bloodcampList= new ArrayList<BloodCamp>();
    }
    
     public BloodCamp newbloodcamp()
    {
        BloodCamp blood = new BloodCamp();
        bloodcampList.add(blood);
        return blood;
    }
    
     public int totalUnitsofblood(){
   int sumA=0;
   for(BloodCamp blood: bloodcampList )
   {
     String bl= String.valueOf(blood.getBloodCollected()) ;
       if(!bl.equals(" ")){
       //if (blood.getType()=="A+")
       sumA += blood.getBloodCollected();
       
          }
       
   }
   return sumA;
     }
}
