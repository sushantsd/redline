/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodCamp;


/**
 *
 * @author harshini
 */
public class BloodCamp {
    private int bloodcampid;
    private Site site;
    private String Name;
    private int BloodCollected;
    private int NoOfDonors;
    //private String Date;
  //  private String StartTime;
  //  private String EndTime;
    private int NoOfNurses;
    
     private static int count = 1;

    public int getBloodcampid() {
        return bloodcampid;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getBloodCollected() {
        return BloodCollected;
    }

    public void setBloodCollected(int BloodCollected) {
        this.BloodCollected = BloodCollected;
    }

    public int getNoOfDonors() {
        return NoOfDonors;
    }

    public void setNoOfDonors(int NoOfDonors) {
        this.NoOfDonors = NoOfDonors;
    }

    public int getNoOfNurses() {
        return NoOfNurses;
    }

    public void setNoOfNurses(int NoOfNurses) {
        this.NoOfNurses = NoOfNurses;
    }
    
    public BloodCamp(){
        bloodcampid = count;
        count++;
}
    
}
