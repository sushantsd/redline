/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodCamp;

import Business.UserAccounts.UserAccounts;

/**
 *
 * @author Ali
 */
public class DonorRegistration {
    private int donorId;
    private String donorName;
    private String address;
    private int contact;
    private boolean donated; //0-no,1-yes
    private boolean tattoo;//0-no,1-yes
    private String gender;
    private int weight;
    private int height;
    private int age;
    private BloodCamp bloodCamp;
    private int hemoglobin;
    private String DonorEmailAddress;
    

    public String getDonorEmailAddress() {
        return DonorEmailAddress;
    }

    public void setDonorEmailAddress(String DonorEmailAddress) {
        this.DonorEmailAddress = DonorEmailAddress;
    }

  

    public int getHemoglobin() {
        return hemoglobin;
    }

    public void setHemoglobin(int hemoglobin) {
        this.hemoglobin = hemoglobin;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }
    private int pressure;
    
    private static int count = 1;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getDonorId() {
        return donorId;
    }

    public void setDonorId(int donorId) {
        this.donorId = donorId;
    }

    public String getDonorName() {
        return donorName;
    }

    public void setDonorName(String donorName) {
        this.donorName = donorName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public boolean isDonated() {
        return donated;
    }

    public void setDonated(boolean donated) {
        this.donated = donated;
    }

    public boolean isTattoo() {
        return tattoo;
    }

    public void setTattoo(boolean tattoo) {
        this.tattoo = tattoo;
    }

   
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public BloodCamp getBloodCamp() {
        return bloodCamp;
    }

    public void setBloodCamp(BloodCamp bloodCamp) {
        this.bloodCamp = bloodCamp;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        DonorRegistration.count = count;
    }
    
    public DonorRegistration(){
         donorId = count;
        count++;
        this.bloodCamp= new BloodCamp();
    }

    public int getDonorid() {
        return donorId;
    }

  
    @Override
    public String toString() {
        return donorName;
    }

  

  
}
