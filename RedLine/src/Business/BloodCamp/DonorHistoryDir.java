/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodCamp;

import java.util.ArrayList;

/**
 *
 * @author Ali
 */
public class DonorHistoryDir {
     private ArrayList<DonorHistory> donorHistoryList;
        
    public DonorHistoryDir(){
        this.donorHistoryList= new ArrayList<DonorHistory>();
    }

    public ArrayList<DonorHistory> getDonorHistoryList() {
        return donorHistoryList;
    }

    public void setDonorHistoryList(ArrayList<DonorHistory> donorHistoryList) {
        this.donorHistoryList = donorHistoryList;
    }
 
     public DonorHistory addDonorhistory() {
        DonorHistory dr = new DonorHistory();
        donorHistoryList.add(dr);
        return dr;
    }
}
