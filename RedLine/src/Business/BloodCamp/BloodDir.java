/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodCamp;

import java.util.ArrayList;

/**
 *
 * @author Ali
 */
public class BloodDir {
   
  private ArrayList<Blood> bloodList;
   
  
  public BloodDir() {
         this.bloodList= new ArrayList<>();
    }
    
   

    public ArrayList<Blood> getBloodList() {
        return bloodList;
    }

    public void setBloodList(ArrayList<Blood> bloodList) {
        this.bloodList = bloodList;
    }
    
    public int totalUnits(String type){
   int sumA=0;
   for(Blood blood: bloodList)
   {
       if(type.equalsIgnoreCase(blood.getType()))
       //if (blood.getType()=="A+")
       sumA += blood.getUnits();
       
          }
   
  //System.out.println(sum);
  return sumA;
 }
    
     public Blood newblood()
    {
        Blood blood = new Blood();
        bloodList.add(blood);
        return blood;
    }
    
}
