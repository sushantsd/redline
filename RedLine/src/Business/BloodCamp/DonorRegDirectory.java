/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodCamp;

import java.util.ArrayList;

/**
 *
 * @author Shreya R
 */
public class DonorRegDirectory {
       private ArrayList<DonorRegistration> donorRegList;

    public ArrayList<DonorRegistration> getDonorRegList() {
        return donorRegList;
    }

    public void setDonorRegList(ArrayList<DonorRegistration> donorRegList) {
        this.donorRegList = donorRegList;
    }
    
     public DonorRegistration addDonor() {
        DonorRegistration dr = new DonorRegistration();
        donorRegList.add(dr);
        return dr;
    }

    public DonorRegDirectory() {
        
        this.donorRegList=new ArrayList<DonorRegistration>();
    }
    
    
    
    
}
