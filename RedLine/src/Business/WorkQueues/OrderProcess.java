/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueues;

/**
 *
 * @author Ali
 */
public class OrderProcess extends WorkRequest {
      private int requestUnits;
    private boolean add;
    
    public OrderProcess(){
        add = false;
    }
    
    public int getQuantity() {
        return requestUnits;
    }

    public void setQuantity(int quantity) {
        this.requestUnits = quantity;
    }

    public boolean isAdd() {
        return add;
    }

    public void setAdd(boolean add) {
        this.add = add;
    }
}
