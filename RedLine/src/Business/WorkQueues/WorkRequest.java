/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueues;

import Business.BloodCamp.Blood;
import Business.BloodCamp.DonorHistory;
import Business.BloodCamp.DonorRegDirectory;
import Business.BloodCamp.DonorRegistration;
import Business.UserAccounts.UserAccounts;
import java.util.Date;

/**
 *
 * @author Shreya R
 */

public abstract class WorkRequest {
    private String message;
    private UserAccounts sender;
    private UserAccounts receiver;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private Blood blood;
     private DonorRegistration donordetails;
    private DonorRegDirectory donorregis;
    private DonorHistory donorhistory;
   // private Vaccines vaccine;

    public DonorHistory getDonorhistory() {
        return donorhistory;
    }

    public void setDonorhistory(DonorHistory donorhistory) {
        this.donorhistory = donorhistory;
    }

    public DonorRegDirectory getDonorregis() {
        return donorregis;
       
    }

    public void setDonorregis(DonorRegDirectory donorregis) {
        this.donorregis = donorregis;
    }

    public DonorRegistration getDonordetails() {
        return donordetails;
    }

    public void setDonordetails(DonorRegistration donordetails) {
        this.donordetails = donordetails;
    }

    public Blood getBlood() {
        return blood;
    }

    public void setBlood(Blood blood) {
        this.blood = blood;
    }

    public UserAccounts getSender() {
        return sender;
    }

    public void setSender(UserAccounts sender) {
        this.sender = sender;
    }

    public UserAccounts getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccounts receiver) {
        this.receiver = receiver;
    }

    
    public WorkRequest(){
        requestDate = new Date();
        blood = new Blood();
        sender = new UserAccounts();
        receiver = new UserAccounts();
        donordetails= new DonorRegistration();
        donorregis= new DonorRegDirectory();
        donorhistory = new DonorHistory();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }
    @Override
    public String toString(){
        return this.status;
       // return donordetails.getDonorName() ;
       
    }

    
}
