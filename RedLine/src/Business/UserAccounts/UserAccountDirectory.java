/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccounts;

import Business.Person.Person;
import Business.Role.Role;
import java.util.ArrayList;




public class UserAccountDirectory {
    private ArrayList<UserAccounts> useraccountList;

    public ArrayList<UserAccounts> getUseraccountList() {
        return useraccountList;
    }

    public void setUseraccountList(ArrayList<UserAccounts> useraccountList) {
        this.useraccountList = useraccountList;
    }

    public UserAccountDirectory()
    {
        this.useraccountList= new ArrayList<UserAccounts>();
    }
    
     public UserAccounts authenticateUser(String username, String password){
        for (UserAccounts ua : useraccountList)
            if (ua.getUserName().equals(username) && ua.getPassword().equals(password)){
                return ua;
            }
        return null;
    }
    
     
      public UserAccounts createUserAccount(String username, String password, Person person, Role role){
        UserAccounts userAccount = new UserAccounts();
        userAccount.setUserName(username);
        userAccount.setPassword(password);
        userAccount.setPerson(person);
        userAccount.setRole(role);
        useraccountList.add(userAccount);
        return userAccount;
    }
     public boolean checkIfUsernameIsUnique(String username){
        for (UserAccounts ua : useraccountList){
            if (ua.getUserName().equals(username))
                return false;
        }
 
        return true;
     }
}
