/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccounts;

import Business.Person.Person;
import Business.Role.Role;
import Business.WorkQueues.WorkQueue;




public class UserAccounts {
    
    private String UserName;
    private String Password;
    private Role role;
    private Person person;
    
    public UserAccounts() {
        workQueue = new WorkQueue();
    }
  

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }
    private WorkQueue workQueue;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
   

 

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }
    
      
    
    @Override
    public String toString() {
        return UserName;
    }
    
}
