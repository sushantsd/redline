/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodBank;

import java.util.ArrayList;

/**
 *
 * @author harshini
 */
public class BloodBankDir {
    private ArrayList<BloodBank> bloodbankList;

    public ArrayList<BloodBank> getBloodbankList() {
        return bloodbankList;
    }

    public void setBloodbankList(ArrayList<BloodBank> bloodbankList) {
        this.bloodbankList = bloodbankList;
    }
    
    public BloodBankDir()
    {
        this.bloodbankList= new ArrayList<BloodBank>();
    }
      public BloodBank newbloodbank()
    {
        BloodBank blood = new BloodBank();
        bloodbankList.add(blood);
        return blood;
    }

}
