/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.BloodBank;

import Business.BloodCamp.BloodDir;
import Business.BloodCamp.Site;
import Business.WorkQueues.WorkQueue;

/**
 *
 * @author harshini
 */
public class BloodBank {
    private int BloodBankid;
    private String Name;
    private Site site;
     private static int count = 1;
     // private WorkQueue workQueue;
      private BloodDir bloodDir;

    public BloodDir getBloodDir() {
        return bloodDir;
    }

    public void setBloodDir(BloodDir bloodDir) {
        this.bloodDir = bloodDir;
    }

   
    public int getBloodBankid() {
        return BloodBankid;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }
    
      public BloodBank(){
        BloodBankid = count;
        count++;
      //  this.workQueue=new WorkQueue();
        this.bloodDir=new BloodDir();
}
}
