/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Person;

import java.util.ArrayList;




public class PersonDirectory {
    
     private ArrayList<Person> personList;

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }

    public PersonDirectory() {
        personList = new ArrayList<>();
    }

   
    
    public Person createperson(String name){
        Person person = new Person();
        person.setName(name);
        personList.add(person);
        return person;
    }
    
}
