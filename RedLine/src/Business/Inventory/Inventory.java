/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Inventory;

import Business.BloodCamp.Blood;
import Business.BloodCamp.BloodDir;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Role.Role;


import java.util.ArrayList;

/**
 *
 * @author Shreya R
 */

public class Inventory {
  
    private Blood blood;
   private BloodDir bd;

    public Blood getBlood() {
        return blood;
    }

    public void setBlood(Blood blood) {
        this.blood = blood;
    }

    public BloodDir getBd() {
        return bd;
    }

    public void setBd(BloodDir bd) {
        this.bd = bd;
    }
    private int quantity;
  
    private Network network;
    private String role;

   
    public Inventory() {
        bd=new BloodDir();
        blood=new Blood();
    }

   
   

    @Override
    public String toString() {
        return blood.getType();
    }

    
    
}
