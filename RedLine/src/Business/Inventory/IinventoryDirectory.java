/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Inventory;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Role.Role;

import java.util.ArrayList;

/**
 *
 * @author Shreya R
 */

public class IinventoryDirectory {
    
     private ArrayList<Inventory> inventoryList;

    public IinventoryDirectory() {
        inventoryList = new ArrayList<Inventory>();
    }

    public ArrayList<Inventory> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(ArrayList<Inventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

    public Inventory addinventory()
    {
        Inventory invent = new Inventory();
        inventoryList.add(invent);
        return invent;
    }
    
}
