/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.BloodBank.BloodBankDir;
import Business.BloodCamp.BloodCampDir;
import Business.BloodCamp.BloodDir;
import Business.BloodCamp.DonorHistory;
import Business.BloodCamp.DonorHistoryDir;
import Business.BloodCamp.DonorRegDirectory;
import Business.BloodCamp.DonorRegistration;

import Business.Inventory.Inventory;
import Business.Network.Network;
import Business.Network.NetworkDirectory;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Person.PersonDirectory;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import Business.UserAccounts.UserAccountDirectory;

import Business.WorkQueues.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author Shreya R
 */


public class Systemm extends Organization {
    
    private static Systemm business;
   
    private NetworkDirectory networkDirectory;
  
    private BloodDir bloodDir;
    private Inventory inventory;  
    private DonorHistoryDir donorhistroydir;
    private UserAccountDirectory userDir;  
    private OrganizationDirectory orgDir;
    private BloodBankDir bbdir;
    private BloodCampDir bcdir;

    public BloodCampDir getBcdir() {
        return bcdir;
    }

    public void setBcdir(BloodCampDir bcdir) {
        this.bcdir = bcdir;
    }

    public BloodBankDir getBbdir() {
        return bbdir;
    }

    public void setBbdir(BloodBankDir bbdir) {
        this.bbdir = bbdir;
    }
    private WorkQueue workQueue;
    private DonorRegDirectory donorRegDir;
 

    public DonorHistoryDir getDonorhistroydir() {
        return donorhistroydir;
    }

    public void setDonorhistroydir(DonorHistoryDir donorhistroydir) {
        this.donorhistroydir = donorhistroydir;
    }

   
    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public BloodDir getBloodDir() {
        return bloodDir;
    }

    public void setBloodDir(BloodDir bloodDir) {
        this.bloodDir = bloodDir;
    }
  

  

    public DonorRegDirectory getDonorRegDir() {
        return donorRegDir;
    }

    public void setDonorRegDir(DonorRegDirectory donorRegDir) {
        this.donorRegDir = donorRegDir;
    }

  

   


    public NetworkDirectory getNetworkDirectory() {
        return networkDirectory;
    }

    public void setNetworkDirectory(NetworkDirectory networkDirectory) {
        this.networkDirectory = networkDirectory;
    }
    
    public OrganizationDirectory getOrgDir() {
        return orgDir;
    }

    public void setOrgDir(OrganizationDirectory orgDir) {
        this.orgDir = orgDir;
    }

  

    public UserAccountDirectory getUserDir() {
        return userDir;
    }

    public void setUserDir(UserAccountDirectory userDir) {
        this.userDir = userDir;
    }

  

  

    public static Systemm getInstance() {
        if (business == null) {
            business = new Systemm();
        }
        return business;
    }

    private Systemm() {
        super(null);
     networkDirectory = new NetworkDirectory();
    
    // vaccineDir= new BloodDir();
     userDir= new UserAccountDirectory();
     orgDir=new OrganizationDirectory();
     workQueue = new WorkQueue();
     donorRegDir=new DonorRegDirectory();
     bloodDir=new BloodDir();
     inventory=new Inventory();
     donorhistroydir = new DonorHistoryDir();
     bbdir= new BloodBankDir();
     bcdir= new BloodCampDir();
     
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

    public boolean checkIfUsernameIsUnique(String username) {

        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            return false;
        }

       

        return true;
    }
} 



    
    
    

