/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;


/**
 *
 * @author Shreya R
 */

public class EnterpriseDirectory {
     private ArrayList<Enterprise> enterpriseList;

    public EnterpriseDirectory() {
        enterpriseList = new ArrayList<>();
    }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }
    
    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if (type == Enterprise.EnterpriseType.BloodBank){
            enterprise = new BloodBankEnterprise(name);
            enterpriseList.add(enterprise);
        }
        
        else if(type == Enterprise.EnterpriseType.BloodCamp){
            enterprise = new BloodCampEnterprise(name);
            enterpriseList.add(enterprise);
        }
         else if(type == Enterprise.EnterpriseType.HealthCare){
            enterprise = new HealthCareEnterprise(name);
            enterpriseList.add(enterprise);
        }
     
        
        return enterprise;
    }
    
    
}
