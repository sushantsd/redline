/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.RequestManagerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author susha
 */
public class RequestManagerOrganization extends Organization{

    public RequestManagerOrganization() {
        super(Organization.Type.RequestManager.getValue());
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new RequestManagerRole());
        return roles;
    }
    
}
