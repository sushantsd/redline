/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.BCEnterpriseAdmin;
import Business.Role.Role;
import java.util.ArrayList;


/**
 *
 * @author Shreya R
 */

public class AdminbcOrganization extends Organization {
    
     public AdminbcOrganization() {
        super(Type.Adminbc.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new BCEnterpriseAdmin());
        return roles;
    }
    
}
