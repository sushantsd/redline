/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.BBEnterpriseAdmin;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import java.util.ArrayList;

/**
 *
 * @author Shreya R
 */


public class AdminbbOrganization extends Organization {
     
     public AdminbbOrganization() {
        super(Type.Adminbb.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new BBEnterpriseAdmin());
        return roles;
    }
    
}
