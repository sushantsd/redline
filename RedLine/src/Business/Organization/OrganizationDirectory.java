/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;




public class OrganizationDirectory {
    
     private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.Adminbb.getValue())){
            organization = new AdminbbOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.Adminbc.getValue())){
            organization = new AdminbcOrganization();
            organizationList.add(organization);
        }
         else if (type.getValue().equals(Organization.Type.Adminhc.getValue())){
            organization = new AdminhcOrganization();
            organizationList.add(organization);
        }
      
        else if (type.getValue().equals(Organization.Type.Donor.getValue())){
            organization = new DonorOrganization();
            organizationList.add(organization);
        }
         else if (type.getValue().equals(Organization.Type.Doctor.getValue())){
            organization = new DoctorOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Organization.Type.Nurse.getValue())){
            organization = new NurseOrganization();
            organizationList.add(organization);
        }
         else if (type.getValue().equals(Organization.Type.Lab.getValue())){
            organization = new LabOrganization();
            organizationList.add(organization);
        }
         else if (type.getValue().equals(Organization.Type.Hospital.getValue())){
            organization = new HospitalOrganization();
            organizationList.add(organization);
        }
         else if (type.getValue().equals(Organization.Type.RequestManager.getValue())){
            organization = new RequestManagerOrganization();
            organizationList.add(organization);
        }
       
        else if (type.getValue().equals(Organization.Type.Order.getValue())){
            organization = new OrderManagerOrganization();
            organizationList.add(organization);
        }
        
        return organization;
    }
    
}