/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Inventory.Inventory;
import Business.Person.PersonDirectory;
import Business.Role.Role;
import Business.UserAccounts.UserAccountDirectory;
import Business.WorkQueues.WorkQueue;
import java.util.ArrayList;




public abstract class Organization {
    
    private String name;
   
    private PersonDirectory personDirectory;
    private WorkQueue workQueue;
     private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter=0;
   
    
    
    public abstract ArrayList<Role> getSupportedRole();

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

   
   
    
    public enum Type{
       Adminbb("Adminbb Organization"),Adminbc("Adminbc Organization"),Adminhc("Adminhc Organization"),Admint("Admint Organization"), Doctor("Doctor Organization"), Lab("Lab Organization"),Inventory("Inventory organization"),
        BloodTransportation("Transport Organization"), RequestManager("Request Manager Organization"),Nurse("Nurse Organization"), Donor("Donor Organization"),Hospital("Hospital Organization"),Order("Order Manage Organization");
         private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name; 
        personDirectory = new PersonDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        workQueue = new WorkQueue();
        
        
        ++counter;
    }

    
    
     public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

  
    
    public String getName() {
        return name;
    }

   

    public void setName(String name) {
        this.name = name;
    }

  

    @Override
    public String toString() {
        return name;
    }
    
}
