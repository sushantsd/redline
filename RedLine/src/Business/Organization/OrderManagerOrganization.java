/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.BloodBank.BloodBank;
import Business.BloodBank.BloodBankDir;
import Business.Role.OrderManagerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Ali
 */
public class OrderManagerOrganization extends Organization{

     private BloodBankDir bbdir;
     private BloodBank bloodBank;

    public BloodBank getBloodBank() {
        return bloodBank;
    }

    public void setBloodBank(BloodBank bloodBank) {
        this.bloodBank = bloodBank;
    }

    public BloodBankDir getBbdir() {
        return bbdir;
    }

    public void setBbdir(BloodBankDir bbdir) {
        this.bbdir = bbdir;
    }

   
    
    
    public OrderManagerOrganization() {
        super(Organization.Type.Order.getValue());
        this.bbdir=new BloodBankDir();
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new OrderManagerRole());
        return roles;
    }
    
    
}
