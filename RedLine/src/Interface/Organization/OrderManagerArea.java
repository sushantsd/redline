/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Organization;

import Business.BloodBank.BloodBank;
import Business.BloodCamp.Blood;
import Business.Enterprise.Enterprise;

import Business.Inventory.Inventory;
import Business.Organization.OrderManagerOrganization;
import Business.Systemm;
import Business.UserAccounts.UserAccounts;
import Business.WorkQueues.BloodOrder;
import Business.WorkQueues.OrderProcessingRequest;
import Business.WorkQueues.WorkQueue;
import Business.WorkQueues.WorkRequest;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Ali
 */
public class OrderManagerArea extends javax.swing.JPanel {

    /**
     * Creates new form OrderManagerArea
     */
 
      private JPanel userProcessContainer;
    private OrderManagerOrganization organization;
    private Enterprise enterprise;
    private UserAccounts userAccount;
    private Systemm business;
    private BloodBank bloodbank;
     public Map<String,Integer> hm;
    private Inventory id;
    

    public OrderManagerArea(JPanel userProcessContainer, UserAccounts account, OrderManagerOrganization orderManagerOrganization, Enterprise enterprise, Systemm business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.organization = orderManagerOrganization;
        this.business=business;
       hashmap();
       populateAvailable();
      
       printBarChart();
     
        populateWorkQueueTable();
   
    }
   
 
     
    public void populateWorkQueueTable(){
         DefaultTableModel model = (DefaultTableModel) requestTable.getModel();
        
        model.setRowCount(0);
        
        
        
        for (WorkRequest work : business.getWorkQueue().getWorkRequestList()){
           if(work instanceof BloodOrder){ 
            Object[] row = new Object[5];
            row[0] = work.getBlood().getType();
            row[1] = ((BloodOrder) work).getQuantity();
            row[2] = work;
            row[3] = work.getReceiver();
            row[4] = work.getSender();
            model.addRow(row);
           }
        }
    }
  
    public void populateAvailable(){
         DefaultTableModel model = (DefaultTableModel) availableTable.getModel();
        
        model.setRowCount(0);
        for(Map.Entry<String, Integer> entry : hm.entrySet())
{
     Object row[]=new Object[2];
    row[0] = entry.getKey();
    row[1]= entry.getValue();
    model.addRow(row);
}
        
        
}
    private void hashmap(){
    hm= new HashMap<String,Integer>();
for(Blood blood:business.getInventory().getBd().getBloodList())
{
hm.put(blood.getType(),business.getInventory().getBd().totalUnits(blood.getType()));
}
}
private void printBarChart(){
            DefaultCategoryDataset barChartData = new DefaultCategoryDataset();
            for(Map.Entry<String, Integer> entry : hm.entrySet()){
                System.out.println(entry.getKey());
         

            
                barChartData.addValue(entry.getValue(), "BloodType", entry.getKey());
                
            }
        JFreeChart barChart = ChartFactory.createBarChart("Blood Inventory", "BloodType", "Units",barChartData, PlotOrientation.VERTICAL, false, true, false);
        
        CategoryPlot barChrt = barChart.getCategoryPlot();
        barChrt.setRangeGridlinePaint(Color.BLUE);
        
        
        ChartPanel panel = new ChartPanel(barChart);
        chartFrame.removeAll();
        chartFrame.add(panel, BorderLayout.CENTER);
        chartFrame.validate();
            
            
        }
        
//        for (Blood blood : business.getInventory().getBd().getBloodList()){
//          
//            Object[] row = new Object[2];
//            row[0] = blood.getType();
//            row[1] = blood.getUnits();
//            model.addRow(row);
           
        
    
     
   /*   public void populateQuantity(){
         
         for ( WorkRequest workRequest : provider.getWorkQueue().getWorkRequestList()) {
            // HashMap<WorkRequest,Integer> map = new HashMap<WorkRequest,Integer>();
             int temp=0;
             ManufacturerWorkRequest p= (ManufacturerWorkRequest) workRequest;
             if(workRequest.getStatus().equals("Complete") && !p.isAdd() ){ //&& add == false
                 Vaccines v = workRequest.getVaccine();
                
                 
                
                  for (Vaccines vaccine : provider.getVaccineList().getVaccineList()) {
                     if(v.getVaccinename().equals(vaccine.getVaccinename())){
                         temp=1;
                          vaccine.setAvailable(p.getQuantity()+vaccine.getAvailable());
                     }
                     
                 }
                  if(temp==0){
                       Vaccines vac= provider.getVaccineList().newvaccine();
                 vac.setDisease(v.getDisease());
                 vac.setVaccinename(v.getVaccinename());
                      vac.setAvailable(p.getQuantity());
                    //  organization.getP().getVaccine().getVaccineList().add(v);
                  }
                 p.setAdd(true);
                  
             }
           //  account.getWorkQueue().getWorkRequestList().remove(workRequest);
         }
      }*/

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        requestTable = new javax.swing.JTable();
        btnDelete = new javax.swing.JButton();
        reqBtn = new javax.swing.JButton();
        btnComplete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        availableTable = new javax.swing.JTable();
        chartFrame = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(199, 231, 247));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        requestTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Vaccine Name", "Quantity", "Status", "Receiver", "Sender"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(requestTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(62, 41, 500, 90));

        btnDelete.setText("Delete request");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(74, 149, -1, -1));

        reqBtn.setText("Assign To Me");
        reqBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reqBtnActionPerformed(evt);
            }
        });
        add(reqBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(216, 149, 120, -1));

        btnComplete.setText("Complete");
        btnComplete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompleteActionPerformed(evt);
            }
        });
        add(btnComplete, new org.netbeans.lib.awtextra.AbsoluteConstraints(354, 149, 140, -1));

        availableTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "BloodType", "Units"
            }
        ));
        jScrollPane1.setViewportView(availableTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 50, 280, 128));
        add(chartFrame, new org.netbeans.lib.awtextra.AbsoluteConstraints(45, 199, 1010, 510));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setText("BLOOD INVENTORY");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 10, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        int selectedRow= requestTable.getSelectedRow();
        if(selectedRow<0){
            JOptionPane.showMessageDialog(null, "Please select the row to delete the account", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else{

            BloodOrder p=(BloodOrder) requestTable.getValueAt(selectedRow, 2);

            business.getWorkQueue().getWorkRequestList().remove(p);

            //  business.getWorkQueue().getWorkRequestList().remove(p);

            JOptionPane.showMessageDialog(null, "You have successfully deleted the account");
            populateWorkQueueTable();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void reqBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reqBtnActionPerformed
        int selectedRow= requestTable.getSelectedRow();
        if(selectedRow<0){
            JOptionPane.showMessageDialog(null, "Please select the row to assign the account", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else{

            BloodOrder p=(BloodOrder) requestTable.getValueAt(selectedRow, 2);

            p.setStatus("Pending");
            p.setReceiver(userAccount);

            populateWorkQueueTable();

        }
     populateWorkQueueTable();

    }//GEN-LAST:event_reqBtnActionPerformed

    private void btnCompleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompleteActionPerformed
        // TODO add your handling code here:
        
        int selectedRow= requestTable.getSelectedRow();
        if(selectedRow<0){
            JOptionPane.showMessageDialog(null, "Please select the row to assign the account", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        else{

            BloodOrder p=(BloodOrder) requestTable.getValueAt(selectedRow, 2);
           int temp=0;
           if(p.getReceiver()!= null){
               if (p.getStatus().equals("Pending")) {
                   UserAccounts a =p.getSender();
                   
                   if(business.getInventory().getBd().getBloodList().size()<= 0){
                 //  if(bloodbank.getBloodDir().getBloodList().size()<= 0){
                       JOptionPane.showMessageDialog(null, "No Stock Available");
                       return;
                   }
                   for (Blood b : business.getInventory().getBd().getBloodList()) {
                       if(p.getBlood().getType().equals(b.getType())){
                           if(b.getUnits()- p.getQuantity()<0){
                               JOptionPane.showMessageDialog(null, "Not enough Blood for supply. Wait for sometime");
                               return;
                           }
                           temp=1;
                           b.setUnits(b.getUnits()- p.getQuantity());
                           break;
                       }

                   }
                   if(temp==0){
                       JOptionPane.showMessageDialog(null, "No Stock available.");
                       return;
                   }

                   p.setStatus("Completed");
                   JOptionPane.showMessageDialog(null, "You have successfully completed the request");
                   populateAvailable();
                   populateWorkQueueTable();
               } else {
                   JOptionPane.showMessageDialog(null, "You cannot complete it two times.");
               }
           }
           else{
               JOptionPane.showMessageDialog(null, "Please assign first");
           }
        //   populateAvailable();
           

       }
         /*   else{
                JOptionPane.showMessageDialog(null, "Please assign first");
            } */

        
    }//GEN-LAST:event_btnCompleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable availableTable;
    private javax.swing.JButton btnComplete;
    private javax.swing.JButton btnDelete;
    private javax.swing.JPanel chartFrame;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton reqBtn;
    private javax.swing.JTable requestTable;
    // End of variables declaration//GEN-END:variables
}
