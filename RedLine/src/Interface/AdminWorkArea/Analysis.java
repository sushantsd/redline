/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.AdminWorkArea;

import Business.BloodCamp.BloodCamp;
import Business.Systemm;
import Business.WorkQueues.BloodOrder;
import Business.WorkQueues.WorkRequest;
import Validations.Utilities;
import java.awt.CardLayout;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Ali
 */
public class Analysis extends javax.swing.JPanel {

    /**
     * Creates new form Analysis
     */
     private JPanel userProcessContainer;
    private Systemm system;
    public Map<String,Integer> hm;
    ImageIcon less = new ImageIcon(getClass().getResource("/Images/less.png"));
    ImageIcon thankyou = new ImageIcon(getClass().getResource("/Images/thankyou.png"));
    Utilities ui;
    Analysis(JPanel userProcessContainer, Systemm business) {
         initComponents();
          this.userProcessContainer = userProcessContainer;
           this.system=business;
        hashmap();
        populatedata();
        counting();
       // image();
        //jLabel6.setEnabled(false);
        
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
     
    public void populatedata(){
        for(Map.Entry<String, Integer> entry : hm.entrySet())
{
    
    jLabel3.setText(String.valueOf(entry.getValue()));
}
    }
    
    private void hashmap(){
   hm= new HashMap<String,Integer>();
for(BloodCamp blood:system.getBcdir().getBloodcampList())
{
hm.put(blood.getName(),system.getBcdir().totalUnitsofblood());
}
    } 
    
    private int counting()
    {
        int amt=0;
        for (WorkRequest work : system.getWorkQueue().getWorkRequestList()){
           if(work instanceof BloodOrder){ 
               if(work.getStatus().equals("Completed"))
               {
                   amt=amt+((BloodOrder) work).getQuantity();
               }
           }
        }
        jLabel5.setText(String.valueOf(amt));
        return amt;
        
    }
 
public void image(){


       
       int collection =Integer.parseInt(jLabel3.getText());
      int distribution = Integer.parseInt(jLabel5.getText());
      System.out.println(collection);
      System.out.println(distribution);
      int difference = distribution-collection;
      if(difference >0){
          if((difference <= 20) || (difference >= 40))
          {
          JOptionPane.showMessageDialog(null, "Increase the Bloodcamps by 20 % to match the requirements");
  //    ui.setClassiclabels(jLabel6,"Blood Requirements Satisfied",thankyou);
          }
          else if((difference <= 41)|| (difference>= 60) )
          {
              JOptionPane.showMessageDialog(null, "Increase the Bloodcamps by 40 % to match the requirements");
          }
           else if((difference <= 61) || (difference >= 80) )
          {
              JOptionPane.showMessageDialog(null, "Increase the Bloodcamps by 60 % to match the requirements");
          }
      }
      else
      {
           JOptionPane.showMessageDialog(null, "Blood Requirements matched");

        //  ui.setClassiclabels(jLabel6,"Blood Requirements not Satisfied",less);
      }
}
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        jLabel4.setText("jLabel4");

        setBackground(new java.awt.Color(199, 231, 247));

        jLabel1.setText("Blood Collected:");

        jLabel2.setText("Blood Distributed:");

        jLabel3.setText("jLabel3");

        jLabel5.setText("jLabel5");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/back.jpg"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel6.setText("jLabel6");

        jButton2.setText("Analyse");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 138, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addGap(82, 82, 82))
            .addGroup(layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(90, 90, 90))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5))
                .addGap(84, 84, 84)
                .addComponent(jLabel6)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(203, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        image();
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables
}
