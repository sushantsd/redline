/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validations;

import com.sun.glass.events.KeyEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.InputEvent;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;

/**
 *
 * @author Shreya Rt
 */
public class Utilities {

    public static boolean isTextFieldEmpty(String x) {

        if (x.trim().equals("")) {
            JOptionPane.showMessageDialog(null, "Kindly enter details");
            return true;
        }
        return false;
    }
    
    public void checkPassword(String password, JProgressBar jProgressBar1, JLabel jLabel7) {
        KeyStroke delKey = KeyStroke.getKeyStroke(KeyEvent.VK_BACKSPACE, InputEvent.META_MASK);
        jProgressBar1.setVisible(true);
        jProgressBar1.setStringPainted(true);
        int strengthPercentage = 0;
        String[] partialRegexChecks = {".*[a-z]+.*", // lower
            ".*[A-Z]+.*", // upper
            ".*[\\d]+.*", // digits
            ".*[@#$%]+.*" // symbols
    };
        if (password.equals(delKey)) {
            jProgressBar1.setValue(0);
            jLabel7.setText("Weaker");
        }
        if (password.matches(partialRegexChecks[0])) {
            strengthPercentage += 25;

        }
        if (password.matches(partialRegexChecks[1])) {
            strengthPercentage += 25;

        }
        if (password.matches(partialRegexChecks[2])) {
            strengthPercentage += 25;

        }
        if (password.matches(partialRegexChecks[3])) {
            strengthPercentage += 25;

        }
        if (strengthPercentage > 20) {
            jProgressBar1.setBackground(Color.red);

            jProgressBar1.setValue(strengthPercentage);
            jLabel7.setText("");
        }
        if (password.length() == 0) {
            strengthPercentage += 0;
            jProgressBar1.setValue(0);
            jLabel7.setText("");
        }
        if (strengthPercentage == 0) {
            jProgressBar1.setBackground(Color.red);
            jProgressBar1.setValue(0);
            jLabel7.setText("");
        }
        if (strengthPercentage > 0) {
            jProgressBar1.setValue(25);
            jProgressBar1.setBackground(Color.red);
            jLabel7.setText("Weak");
        }
        if (strengthPercentage > 26) {
            jProgressBar1.setBackground(Color.pink);

            jProgressBar1.setValue(strengthPercentage);
            jLabel7.setText("Fine");
        }
        if (strengthPercentage > 51) {
            jProgressBar1.setBackground(Color.orange);

            jProgressBar1.setValue(strengthPercentage);
            jLabel7.setText("Strong");
        }
        if (strengthPercentage > 76) {
            jProgressBar1.setBackground(Color.green);

            jProgressBar1.setValue(strengthPercentage);
            jLabel7.setText("Very Strong");
        }
    }
    
   public void setClassiclabels(JLabel button,String title, Icon icon){
    //   makeButtonsTransparent(button);
       button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
       button.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
       button.setIcon(icon);
       button.setText(title);
       button.setForeground(Color.black);
   }
  public void setClassicIcons(JButton button,String title, Icon icon){
     //   makeButtonsTransparent(button);
        button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        button.setVerticalTextPosition(javax.swing.SwingConstants.CENTER);
        button.setIcon(icon);
        button.setText(title);
        button.setForeground(Color.black);
    }
    public void setClassicButtons(JButton button,String toolTip, Icon icon){
       // makeButtonsTransparent(button);
        button.setText(null);
        button.setIcon(icon);
        button.setToolTipText(toolTip);
        button.setForeground(Color.black);
        
    }
    public void setHoverIcon(java.awt.event.MouseEvent evt, Icon icon){
        JButton button=(JButton)evt.getSource();
        button.setIcon(icon);
        button.setFont(button.getFont().deriveFont(Font.BOLD));
        button.setForeground(Color.white);
    }
    public void setOffHoverIcon(java.awt.event.MouseEvent evt, Icon icon){
        JButton button=(JButton)evt.getSource();
        button.setIcon(icon);
        button.setFont(button.getFont().deriveFont(Font.PLAIN));
        button.setForeground(Color.black);
    }
    public static boolean isValidNumber(String x) {

        try {
            int tempD = Integer.parseInt(x.trim());
            if (tempD <= 0) {
                JOptionPane.showMessageDialog(null, "Invalid input!\nKindly enter a valid value!");
                return false;
            }
            return true;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Invalid input!\nKindly enter a valid value!");
            return false;
        }
    }

    public static boolean isNameLength4(String text) {
        if (text.trim().length() < 4) {
              JOptionPane.showMessageDialog(null, "String is too short!\n");

            return false;
        } else {
            return true;
        }
    }
     public void makeButtonsTransparent(JButton button) {
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
    }
      public void makeButtonsTransparent(JToggleButton jToggleButton1) {
        jToggleButton1.setOpaque(false);
        jToggleButton1.setContentAreaFilled(false);
       jToggleButton1.setBorderPainted(false);
    }
      
    public static boolean isIntegerPositive(String text) {
        if (text != null && text.length() > 0) {
            Pattern pattern = Pattern.compile("\\b\\d+\\b");
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) {
                if(Long.parseLong(text) > 0){
                     return true;
                }
               
            }
        }
        return false;
    }

    public static boolean isStringValid(String text) {
        if (text != null && text.length() > 0) {
            Pattern pattern = Pattern.compile("^[a-zA-Z0-9_]+$");
            Matcher matcher = pattern.matcher(text);
            if (matcher.matches()) {
                return true;
            }
        }

        return false;
    }

    public static boolean isOnlyString(String text) {
        if (text != null && text.length() > 0) {
            Pattern pattern = Pattern.compile("^[a-zA-Z]+$");
            Matcher matcher = pattern.matcher(text);
            if (matcher.matches()) {
                return true;
            }
        }

        return false;
    }

    public static boolean isEmailValid(String email) {

        if (email != null && email.length() > 0) {
            String regex = "^[A-Za-z0-9._]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,6}$";
            if (email.matches(regex)) {
                return true;
            }

        }
        return false;
    }

    public static boolean isPasswordAndConfirmPasswordSame(String password, String confirm) {

        if (password != null && password.length() > 0 && confirm != null && confirm.length() > 0) {

            if (password.equalsIgnoreCase(confirm)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isMobileNumberValid(String a_number) {

        if (a_number != null && a_number.length() > 0) {

            Pattern pattern = Pattern.compile("\"\\\\d{3}-\\\\d{7}\"");
            Matcher matcher = pattern.matcher(a_number);
            if (matcher.matches()) {
                return true;
            }
        }

        return false;
    }

    public static boolean isMonthValid(String a_text) {

        if (isIntegerPositive(a_text)) {
            int mm = Integer.parseInt(a_text);
            if (mm > 0 && mm <= 12) {
                return true;
            }
        }

        return false;
    }

    public static boolean isDateValid(String a_text) {

        if (isIntegerPositive(a_text)) {
            int dd = Integer.parseInt(a_text);
            if (dd > 0 && dd <= 31) {
                return true;
            }
        }

        return false;
    }

    public static boolean isYearValid(String a_text) {

        if (isIntegerPositive(a_text)) {
            int yyyy = Integer.parseInt(a_text);
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            if (yyyy > 1900 && yyyy <= currentYear) {
                return true;
            }
        }

        return false;
    }


public static boolean isStringValidWithSpace(String text) {
        if (text != null && text.length() > 0) {
            Pattern pattern = Pattern.compile("^[a-zA-Z0-9_ ]+$");
            Matcher matcher = pattern.matcher(text);
            if (matcher.matches()) {
                return true;
            }
        }

        return false;
    }

public static boolean isStringValidContact(String text) {
        if (text != null && text.length() > 0) {
            Pattern pattern = Pattern.compile("^[0-9]+$");
            Matcher matcher = pattern.matcher(text);
            if (matcher.matches()) {
                return true;
            }
        }

        return false;
    }

   

    //public static String toProperCase(String text) {
     //   return text.substring(0, 1).toUpperCase() + text.substring(1, text.length()).toLowerCase();
    //}

   

  
}
