/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.bloodcamp;

/**
 *
 * @author Ali
 */
import Business.Enterprise.Enterprise;
import Business.Organization.RequestManagerOrganization;
import Business.Systemm;
import Business.UserAccounts.UserAccounts;
import Interface.Organization.RequestManagerArea;
import java.awt.CardLayout;

import javax.swing.JPanel;



   
 
public class mapp extends javax.swing.JPanel {

   
    
     private JPanel userProcessContainer;
    private RequestManagerOrganization organization;
    private Enterprise enterprise;
    private UserAccounts userAccount;
    private Systemm business;
     static final int V=9;
    
     private double long1=42.3601;
     private double lati1=71.0589;
     private double long2=74.0060;
     private double lati2=40.7128;
     private double long3=81.5158;
     private double lati3=27.6648;
     private double long4=122.4194;
     private double lati4=37.7749;
   
    
    public mapp(JPanel userProcessContainer, UserAccounts account, RequestManagerOrganization requestManagerOrganization, Enterprise enterprise, Systemm business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.organization = requestManagerOrganization;
        this.business=business;
         int graph[][] = new int[][]{{0, 4, 0, 0, 0, 0, 0, 8, 0},
                                  {4, 0, 8, 0, 0, 0, 0, 11, 0},
                                  {0, 8, 0, 7, 0, 4, 0, 0, 2},
                                  {0, 0, 7, 0, 9, 14, 0, 0, 0},
                                  {0, 0, 0, 9, 0, 10, 0, 0, 0},
                                  {0, 0, 4, 14, 10, 0, 2, 0, 0},
                                  {0, 0, 0, 0, 0, 2, 0, 1, 6},
                                  {8, 11, 0, 0, 0, 0, 1, 0, 7},
                                  {0, 0, 2, 0, 0, 0, 6, 7, 0}
                                 };
        ShortestPath t = new ShortestPath();
        t.dijkstra(graph, 0);
         
        
    
       
       
    }
    class ShortestPath
{
    // A utility function to find the vertex with minimum distance value,
    // from the set of vertices not yet included in shortest path tree
    static final int V=9;
    int minDistance(int dist[], Boolean sptSet[])
    {
        // Initialize min value
        int min = Integer.MAX_VALUE, min_index=-1;
 
        for (int v = 0; v < V; v++)
            if (sptSet[v] == false && dist[v] <= min)
            {
                min = dist[v];
                min_index = v;
            }
 
        return min_index;
    }
 
    // A utility function to print the constructed distance array
    void printSolution(int dist[], int n)
    {
        System.out.println("Vertex   Distance from Source");
        for (int i = 0; i < V; i++)
            System.out.println(i+" tt "+dist[i]);
    }
 
    // Funtion that implements Dijkstra's single source shortest path
    // algorithm for a graph represented using adjacency matrix
    // representation
    void dijkstra(int graph[][], int src)
    {
        int dist[] = new int[V]; // The output array. dist[i] will hold
                                 // the shortest distance from src to i
 
        // sptSet[i] will true if vertex i is included in shortest
        // path tree or shortest distance from src to i is finalized
        Boolean sptSet[] = new Boolean[V];
 
        // Initialize all distances as INFINITE and stpSet[] as false
        for (int i = 0; i < V; i++)
        {
            dist[i] = Integer.MAX_VALUE;
            sptSet[i] = false;
        }
 
        // Distance of source vertex from itself is always 0
        dist[src] = 0;
 
        // Find shortest path for all vertices
        for (int count = 0; count < V-1; count++)
        {
            // Pick the minimum distance vertex from the set of vertices
            // not yet processed. u is always equal to src in first
            // iteration.
            int u = minDistance(dist, sptSet);
 
            // Mark the picked vertex as processed
            sptSet[u] = true;
 
            // Update dist value of the adjacent vertices of the
            // picked vertex.
            for (int v = 0; v < V; v++)
 
                // Update dist[v] only if is not in sptSet, there is an
                // edge from u to v, and total weight of path from src to
                // v through u is smaller than current value of dist[v]
                if (!sptSet[v] && graph[u][v]!=0 &&
                        dist[u] != Integer.MAX_VALUE &&
                        dist[u]+graph[u][v] < dist[v])
                    dist[v] = dist[u] + graph[u][v];
        }
 
        // print the constructed distance array
        printSolution(dist, V);
    }
    }

    public double distance(double latitude1, double longitute1, double latitude2, double longitute2, char unit) {
     double theta = longitute1 - longitute2;
     double distance = Math.sin(degreetoradian(latitude1)) * Math.sin(degreetoradian(longitute2)) + Math.cos(degreetoradian(latitude1)) * Math.cos(degreetoradian(latitude2)) * Math.cos(degreetoradian(theta));
     distance = Math.acos(distance);
     distance = radiantodegree(distance);
     distance = distance * 60 * 1.1515;
     if (unit == 'K') {
       distance = distance * 1.609344;
     } else if (unit == 'N') {
       distance = distance * 0.8684;
       }
      System.out.println(distance);
     return distance;
       
    }
      private double degreetoradian(double deg) {
     return (deg * Math.PI / 180.0);
   }

   private double radiantodegree(double rad) {
     return (rad * 180.0 / Math.PI);
   }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Request = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(199, 231, 247));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Request.setText("Request Blood");
        Request.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RequestActionPerformed(evt);
            }
        });
        add(Request, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 570, 150, 50));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/493a4eb8d3ac129972f68cb87fa612a7.jpg"))); // NOI18N
        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(29, 464, 350, 180));

        jLabel1.setText("jLabel1");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 390, -1, -1));

        jLabel2.setText("jLabel2");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 380, -1, -1));

        jLabel3.setText("jLabel3");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 430, -1, -1));

        jLabel4.setText("jLabel4");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 680, -1, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/hos1.jpg"))); // NOI18N
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 160, -1, -1));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/hos2.jpg"))); // NOI18N
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 130, -1, -1));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/hos3.png"))); // NOI18N
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 460, -1, -1));

        jLabel8.setText("Blood Bank A");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 380, -1, -1));

        jLabel9.setText("Blood Bank B");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(1090, 380, -1, -1));

        jLabel10.setText("Blood Bank C distance :");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 430, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void RequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RequestActionPerformed
        // TODO add your handling code here:
        
         RequestManagerArea panel = new RequestManagerArea(userProcessContainer, userAccount, (RequestManagerOrganization)organization, enterprise,business);
        userProcessContainer.add("Managerequest", panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_RequestActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        double distance1 =distance(lati2, long2, lati1, long1, 'k');
        double distance2 = distance(lati3, long3, lati1, long1, 'k');
        double distance3=distance(lati4, long4, lati1, long1, 'k');
        
        jLabel1.setText(String.valueOf(distance1));
        jLabel2.setText(String.valueOf(distance2));
        jLabel3.setText(String.valueOf(distance3));
        if (distance3-distance1>0){
           if(distance2-distance1>0)
           {
               jLabel4.setText("The shortest distance is between Hosiptal and Blood Bank A");
           }
           else
           {
               jLabel4.setText("The shortest distance is between Hosiptal and Blood Bank B");
           }
          
        }
         else if(distance3-distance2>0)
           {
                   jLabel4.setText("The shortestdistance is between Hosiptal and Blood Bank C");
                   }
    }//GEN-LAST:event_jButton1ActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Request;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    // End of variables declaration//GEN-END:variables

   
   
    
  
   
}
